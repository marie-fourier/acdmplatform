import { expect } from "chai";
import { ethers } from "hardhat";

async function getTimestamp() {
  const blockNumber = await ethers.provider.getBlockNumber();
  const block = await ethers.provider.getBlock(blockNumber);
  return block.timestamp;
}

async function increaseTime(seconds: number) {
  const timestamp = await getTimestamp();
  await setTimestamp(timestamp + seconds);
}

async function setTimestamp(timestamp: number) {
  await ethers.provider.send("evm_mine", [timestamp]);
}

const zeroAddress = "0x0000000000000000000000000000000000000000";

describe("ACDMPlatform", function () {
  let owner: any, account1: any, account2: any, contract: any, token: any;

  before(async () => {
    const Contract = await ethers.getContractFactory("ACDMPlatform");
    const Token = await ethers.getContractFactory("Token");
    [owner, account1, account2] = await ethers.getSigners();

    token = await Token.deploy();
    await token.deployed();

    contract = await Contract.deploy(token.address, 3 * 24 * 60 * 60);
    await contract.deployed();

    await token.transferOwnership(contract.address);
  });

  describe("Register", async () => {
    it("should register", async () => {
      await expect(contract.register(zeroAddress))
        .to.emit(contract, "NewUser")
        .withArgs(owner.address, zeroAddress, zeroAddress);
    });

    it("should not register twice", async () => {
      await expect(contract.register(zeroAddress)).to.be.revertedWith(
        "Already registered"
      );
    });

    it("should not refer himself", async () => {
      await expect(contract.register(owner.address)).to.be.revertedWith(
        "Can't refer yourself"
      );
    });

    it("should refer", async () => {
      await expect(contract.connect(account1).register(owner.address))
        .to.emit(contract, "NewUser")
        .withArgs(account1.address, owner.address, zeroAddress);
      await expect(contract.connect(account2).register(account1.address))
        .to.emit(contract, "NewUser")
        .withArgs(account2.address, account1.address, owner.address);
    });
  });

  describe("Sale round", async () => {
    it("should not run trade round before sale round", async () => {
      await expect(contract.startTradeRound()).to.be.revertedWith(
        "Unable to start trade round"
      );
    });

    it("should start round", async () => {
      await expect(contract.startSaleRound())
        .to.emit(contract, "SaleRoundStarted")
        .withArgs(
          ethers.utils.parseUnits("10000", "gwei"),
          ethers.utils.parseEther("100000")
        );
    });

    it("should not start round twice", async () => {
      await expect(contract.startSaleRound()).to.be.revertedWith(
        "Unable to start sale round"
      );
    });

    it("buying should transfer tokens", async () => {
      await expect(() =>
        contract.buyACDM({ value: ethers.utils.parseEther("0.1") })
      )
        .to.changeEtherBalance(contract, ethers.utils.parseEther("0.1"))
        .to.changeTokenBalance(token, owner, ethers.utils.parseEther("10000"));
    });

    it("should distribute ether to referrers", async () => {
      await expect(() =>
        contract.buyACDM({ value: ethers.utils.parseEther("0.1") })
      ).to.changeEtherBalance(contract, ethers.utils.parseEther("0.1"));

      await expect(() =>
        contract
          .connect(account1)
          .buyACDM({ value: ethers.utils.parseEther("0.1") })
      ).to.changeEtherBalance(owner, ethers.utils.parseEther("0.005"));

      await expect(() =>
        contract
          .connect(account2)
          .buyACDM({ value: ethers.utils.parseEther("0.1") })
      ).to.changeEtherBalances(
        [account1, owner],
        [ethers.utils.parseEther("0.005"), ethers.utils.parseEther("0.003")]
      );
    });

    it("should not receive more ether than token supply", async () => {
      await expect(
        contract.buyACDM({ value: ethers.utils.parseEther("10") })
      ).to.be.revertedWith("Too much ether");
    });

    it("buying all liquidity should end sale round", async () => {
      await contract.buyACDM({ value: ethers.utils.parseEther("0.5") });
      expect(await token.balanceOf(contract.address)).to.eq(0);
      await expect(
        contract.buyACDM({ value: ethers.utils.parseEther("0.1") })
      ).to.be.revertedWith("Sale round finished");
    });
  });

  describe("Trade round", async () => {
    it("should start round", async () => {
      await expect(contract.startTradeRound()).to.emit(
        contract,
        "TradeRoundStarted"
      );
    });

    it("should not start sale round before trade round finishes", async () => {
      await expect(contract.startSaleRound()).to.be.revertedWith(
        "Unable to start sale round"
      );
    });

    let orderId: any = null;
    it("should add order", async () => {
      orderId = ethers.utils.solidityKeccak256(
        ["address", "uint256", "uint256"],
        [
          owner.address,
          ethers.utils.parseEther("10000"),
          ethers.utils.parseEther("0.5"),
        ]
      );
      await token.approve(contract.address, ethers.utils.parseEther("10000"));
      await expect(
        contract.addOrder(
          ethers.utils.parseEther("10000"),
          ethers.utils.parseEther("0.5")
        )
      )
        .to.emit(contract, "NewOrder")
        .withArgs(
          orderId,
          ethers.utils.parseEther("10000"),
          ethers.utils.parseEther("0.5")
        );
    });

    it("should not redeem more than order liquidity", async () => {
      await expect(
        contract
          .connect(account1)
          .redeemOrder(orderId, { value: ethers.utils.parseEther("10") })
      ).to.be.revertedWith("Too much ether");
    });

    it("should buy fraction of order, distribute referrer rewards and owner rewards", async () => {
      await expect(() =>
        contract.redeemOrder(orderId, { value: ethers.utils.parseEther("0.1") })
      ).to.changeTokenBalance(token, owner, ethers.utils.parseEther("2000"));
      await expect(() =>
        contract
          .connect(account1)
          .redeemOrder(orderId, { value: ethers.utils.parseEther("0.1") })
      ).to.changeEtherBalance(owner, ethers.utils.parseEther("0.0975"));
      await expect(() =>
        contract
          .connect(account2)
          .redeemOrder(orderId, { value: ethers.utils.parseEther("0.1") })
      ).to.changeEtherBalances(
        [account1, owner],
        [ethers.utils.parseEther("0.0025"), ethers.utils.parseEther("0.0975")]
      );
      await expect(
        contract
          .connect(account1)
          .redeemOrder(orderId, { value: ethers.utils.parseEther("0.5") })
      ).to.be.revertedWith("Too much ether");
    });

    it("should remove order and send rest of order to owner", async () => {
      await expect(
        contract.connect(account1).removeOrder(orderId)
      ).to.be.revertedWith("You can't remove this order");
      await expect(() => contract.removeOrder(orderId)).to.changeTokenBalance(
        token,
        owner,
        ethers.utils.parseEther("4000")
      );
    });

    it("should not redeem removed order", async () => {
      await expect(
        contract.redeemOrder(orderId, { value: ethers.utils.parseEther("0.1") })
      ).to.be.revertedWith("Invalid round");
    });
  });

  describe("Round 2", async () => {
    it("should start sale round after trade round with proper supply", async () => {
      await expect(contract.startSaleRound()).to.be.revertedWith(
        "Unable to start sale round"
      );
      await increaseTime(3 * 24 * 60 * 60);
      await expect(contract.startSaleRound())
        .to.emit(contract, "SaleRoundStarted")
        .withArgs(
          ethers.utils.parseEther("0.0000143"), // 2 round price
          ethers.utils.parseEther("20979") // 0.3 eth / 0.0000143 eth
        );
    });

    it("should not redeem removed order", async () => {
      await expect(
        contract.addOrder(
          ethers.utils.parseEther("10000"),
          ethers.utils.parseEther("0.5")
        )
      ).to.be.revertedWith("Not in trade round");
    });
  });

  it("should withdraw", async () => {
    await expect(contract.connect(account1).withdraw()).to.be.reverted;
    const balance = await ethers.provider.getBalance(contract.address);
    await expect(() => contract.withdraw()).to.changeEtherBalance(
      owner,
      balance
    );
  });
});

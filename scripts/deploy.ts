import { ethers } from "hardhat";

async function main() {
  const erc20Address = String(process.env.TOKEN_ADDRESS);
  if (!erc20Address) {
    throw Error("Invalid ERC20 address");
  }
  const Contract = await ethers.getContractFactory("ACDMPlatform");
  const contract = await Contract.deploy(erc20Address, 3 * 24 * 60 * 60); // 3 days

  await contract.deployed();

  console.log("contract deployed to:", contract.address);
  const erc20Token = await ethers.getContractAt(
    "Token",
    String(process.env.TOKEN_ADDRESS)
  );
  try {
    await erc20Token.transferOwnership(contract.address);
    console.log("Transferred ownership of erc20 token");
  } catch (err) {
    console.log("Could not transfer ownership");
  }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

// SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

interface IMintableERC20 is IERC20 {
  function mint(address addr, uint256 amount) external;
  function decimals() external returns (uint8);
}
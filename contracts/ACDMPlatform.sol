// SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./interfaces/IMintableERC20.sol";

contract ACDMPlatform is Ownable {
  event NewUser(address indexed user, address indexed referrer1, address indexed referrer2);
  event SaleRoundStarted(uint256 price, uint256 supply);
  event TradeRoundStarted();
  event NewOrder(uint256 indexed orderId, uint256 tokenAmount, uint256 etherAmount);

  enum Round {
    None,
    Sale,
    Trade
  }
  struct Order {
    address owner;
    uint256 tokenAmount;
    uint256 etherAmount;
    uint256 perTokenPrice;
    uint256 roundStartTime; // round id
  }

  IMintableERC20 private _token;
  Round private _round;

  mapping(address => address) private _referrers;
  mapping(address => bool) private _registeredUsers;
  mapping(uint256 => Order) private _orders;

  uint256 private _roundTime;
  uint256 private _perTokenPrice = 10000 gwei;
  uint256 private _lastRoundVolume = 1 ether;
  uint256 private _roundStartTime;

  constructor(address tokenForSale, uint256 roundTime) {
    _token = IMintableERC20(tokenForSale);
    _roundTime = roundTime;
  }

  function register(address referrer) external {
    require(referrer != msg.sender, "Can't refer yourself");
    require(!_registeredUsers[msg.sender], "Already registered");
    if (_registeredUsers[referrer]) {
      _referrers[msg.sender] = referrer;
    }
    _registeredUsers[msg.sender] = true;
    emit NewUser(msg.sender, _referrers[msg.sender], _referrers[_referrers[msg.sender]]);
  }

  function startSaleRound() external {
    require(_round != Round.Sale && tradeRoundFinished(), "Unable to start sale round");
    _round = Round.Sale;
    uint256 supply = _lastRoundVolume / _perTokenPrice * 10 ** _token.decimals();
    _token.mint(address(this), supply);
    _roundStartTime = block.timestamp;
    emit SaleRoundStarted(_perTokenPrice, supply);
  }

  function buyACDM() external payable {
    require(_round == Round.Sale && !saleRoundFinished(), "Sale round finished");
    uint256 amount = msg.value / _perTokenPrice * 10 ** _token.decimals();
    require(amount <= acdmBalance(), "Too much ether");
    (address ref1, address ref2) = getReferrers(msg.sender);
    distributeSaleRoundReferrerRewards(msg.value, ref1, ref2);
    _token.transfer(msg.sender, amount);
  }

  function startTradeRound() external {
    require(_round == Round.Sale && saleRoundFinished(), "Unable to start trade round");
    _round = Round.Trade;
    _roundStartTime = block.timestamp;
    _lastRoundVolume = 0;
    _perTokenPrice = _perTokenPrice * 1030 / 1000 + 4000 gwei;
    emit TradeRoundStarted();
  }

  function addOrder(uint256 tokensAmount, uint256 etherAmount) external returns (uint256) {
    require(_round == Round.Trade, "Not in trade round");
    uint256 orderId = uint256(
      keccak256(
        abi.encodePacked(msg.sender, tokensAmount, etherAmount)
      )
    );
    _token.transferFrom(msg.sender, address(this), tokensAmount);
    Order storage order = _orders[orderId];
    order.owner = msg.sender;
    order.tokenAmount = tokensAmount;
    order.etherAmount = etherAmount;
    order.perTokenPrice = etherAmount / (tokensAmount / 10 ** _token.decimals());
    order.roundStartTime = _roundStartTime;
    emit NewOrder(orderId, tokensAmount, etherAmount);
    return orderId;
  }

  function removeOrder(uint256 orderId) external {
    require(_orders[orderId].owner == msg.sender, "You can't remove this order");
    uint256 tokensLeft = _orders[orderId].tokenAmount;
    _orders[orderId].owner = address(0);
    _orders[orderId].tokenAmount = 0;
    _orders[orderId].etherAmount = 0;
    _orders[orderId].roundStartTime = 0;
    _token.transfer(msg.sender, tokensLeft);
  }

  function redeemOrder(uint256 orderId) external payable {
    Order storage order = _orders[orderId];
    require(order.roundStartTime == _roundStartTime, "Invalid round");
    uint256 tokenAmount = msg.value / order.perTokenPrice * 10 ** _token.decimals();
    require(tokenAmount <= order.tokenAmount, "Too much ether");
    order.tokenAmount -= tokenAmount;
    (address ref1, address ref2) = getReferrers(msg.sender);
    distributeTradeRoundReferrerRewards(msg.value, ref1, ref2);
    _lastRoundVolume += msg.value;
    _token.transfer(msg.sender, tokenAmount);
    payable(order.owner).transfer(msg.value / 100 * 95);
  }

  function withdraw() external onlyOwner {
    payable(msg.sender).transfer(address(this).balance);
  }

  function acdmBalance() internal view returns (uint256) {
    return _token.balanceOf(address(this));
  }

  function saleRoundFinished() internal view returns (bool) {
    return acdmBalance() == 0 || _roundStartTime + _roundTime <= block.timestamp;
  }

  function tradeRoundFinished() internal view returns (bool) {
    return _roundStartTime + _roundTime <= block.timestamp;
  }

  function getReferrers(address user) internal view returns (address referrer1, address referrer2) {
    referrer1 = _referrers[user];
    if (!_registeredUsers[referrer1]) {
      referrer1 = address(this);
    }
    referrer2 = _referrers[referrer1];
    if (!_registeredUsers[referrer2]) {
      referrer2 = address(this);
    }
  }  

  function distributeSaleRoundReferrerRewards(uint256 value, address referrer1, address referrer2) internal {
    if (referrer1 != address(this) && referrer1 != address(0)) {
      payable(referrer1).transfer(value / 100 * 5);
    }
    if (referrer2 != address(this) && referrer2 != address(0)) {
      payable(referrer2).transfer(value / 100 * 3);
    }
  }

  function distributeTradeRoundReferrerRewards(uint256 value, address referrer1, address referrer2) internal {
    if (referrer1 != address(this) && referrer1 != address(0)) {
      payable(referrer1).transfer(value / 1000 * 25);
    }
    if (referrer2 != address(this) && referrer2 != address(0)) {
      payable(referrer2).transfer(value / 1000 * 25);
    }
  }
}

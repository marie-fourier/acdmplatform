import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  referrer?: string;
}

task("register")
  .addOptionalParam("referrer", "Referrer address")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    if (!taskArgs.referrer) {
      taskArgs.referrer = "0x0000000000000000000000000000000000000000";
    }
    const contract = await getContract(hre);
    await contract.register(taskArgs.referrer);
    console.log("Successfully registered");
  });

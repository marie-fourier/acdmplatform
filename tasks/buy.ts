import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  value: string;
}

task("buy")
  .addParam("value", "Amount of ether")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    await contract.buyACDM({
      value: hre.ethers.utils.parseEther(taskArgs.value),
    });
    console.log("Bought tokens");
  });

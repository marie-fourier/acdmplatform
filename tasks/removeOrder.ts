import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  id: string;
}

task("removeOrder")
  .addParam("id", "Order id")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    await contract.removeOrder(hre.ethers.utils.parseEther(taskArgs.id));
    console.log("Removed order");
  });

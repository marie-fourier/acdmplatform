import * as fs from "fs";

fs.readdir(__dirname, (err, files) => {
  if (err) {
    return;
  }
  for (const file of files) {
    require(`./${file}`);
  }
});

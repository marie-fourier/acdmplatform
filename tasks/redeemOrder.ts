import { task } from "hardhat/config";
import { getAccounts, getContract, getTokenContract } from "../helpers";

interface TaskArgs {
  token: string;
  ether: string;
}

task("redeemOrder")
  .addParam("token", "Amount of ACDM tokens")
  .addParam("ether", "Amount of ether")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    const tokenContract = await getTokenContract(hre);
    const [owner] = await getAccounts(hre);
    await tokenContract.approve(
      contract.address,
      hre.ethers.utils.parseEther(taskArgs.token)
    );
    await contract.addOrder(
      hre.ethers.utils.parseEther(taskArgs.token),
      hre.ethers.utils.parseEther(taskArgs.ether)
    );
    const orderId = hre.ethers.utils.solidityKeccak256(
      ["address", "uint256", "uint256"],
      [
        owner.address,
        hre.ethers.utils.parseEther(taskArgs.token),
        hre.ethers.utils.parseEther(taskArgs.ether),
      ]
    );
    console.log("Created order:", orderId);
  });

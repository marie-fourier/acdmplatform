import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  id: string;
  value: string;
}

task("addOrder")
  .addParam("id", "Order id")
  .addParam("value", "Amount of ether")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    await contract.redeemOrder(hre.ethers.utils.parseEther(taskArgs.id), {
      value: hre.ethers.utils.parseEther(taskArgs.value),
    });
    console.log("Bought tokens");
  });

import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("withdraw").setAction(async (taskArgs, hre) => {
  const contract = await getContract(hre);
  await contract.withdraw();
  console.log("Withdrawn");
});

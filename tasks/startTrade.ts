import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("startTrade").setAction(async (args, hre) => {
  const contract = await getContract(hre);
  await contract.startTradeRound();
  console.log("Started trade round");
});

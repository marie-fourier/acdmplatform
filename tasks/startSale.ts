import { task } from "hardhat/config";
import { getContract } from "../helpers";

task("startSale").setAction(async (args, hre) => {
  const contract = await getContract(hre);
  await contract.startSaleRound();
  console.log("Started sale round");
});
